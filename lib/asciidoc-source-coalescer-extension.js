'use strict'

const Opal = global.Opal

// patch Array to fix line counting-related bug in parser in Asciidoctor 1.5.8
const ArrayExt = (() => {
  const scope = Opal.module(null, 'ArrayExt', function $ArrayExt () {})

  Opal.defn(scope, '$delete_at', function deleteAt (idx) {
    if (~new Error().stack.split('\n')[2].indexOf('.$$read_lines_for_list_item ')) {
      const val = this[idx]
      this[idx] = ''
      return val
    } else {
      return Opal.send(this, Opal.find_super_dispatcher(this, 'delete_at', deleteAt), [idx])
    }
  })

  return scope
})()

const DocumentExt = (() => {
  const parentScope = Opal.module(null, 'AsciiDocSourceCoalescer', function $AsciiDocSourceCoalescer () {})
  const scope = Opal.module(parentScope, 'DocumentExt', function $DocumentExt () {})

  Opal.defn(scope, '$save_attributes', function saveAttributes () {
    this.attributes_defined_in_header = this.attributes_modified.$to_a().reduce((accum, name) => {
      accum[name] = this.getAttribute(name)
      return accum
    }, {})
    Opal.send(this, Opal.find_super_dispatcher(this, 'save_attributes', saveAttributes), [])
  })

  return scope
})()

const PreprocessorReaderExt = (() => {
  const parentScope = Opal.module(null, 'AsciiDocSourceCoalescer', function $AsciiDocSourceCoalescer () {})
  const scope = Opal.module(parentScope, 'PreprocessorReaderExt', function $PreprocessorReaderExt () {})

  Opal.defn(scope, '$preprocess_include_directive', function preprocessIncludeDirective (target, attrlist) {
    this.$$include_directive_line = `include::${target}[${attrlist}]`
    this.$$push_include_called = false
    const result = Opal.send(
      this,
      Opal.find_super_dispatcher(this, 'preprocess_include_directive', preprocessIncludeDirective),
      [target, attrlist]
    )
    if (this.$$push_include_called) return result
    let parentDepth = this.parents.length
    const depthChange = this.include_stack.length - (parentDepth - 1)
    if (depthChange < 0) parentDepth -= this.parents.splice(parentDepth + depthChange, -depthChange).length
    const nextLine = this.lines[0]
    if (nextLine && nextLine.startsWith('Unresolved include directive in ')) {
      this.include_replacements.push({
        lines: [nextLine],
        into: this.parents[parentDepth - 1],
        index: this.lineno - 1,
        replace: this.$$include_directive_line,
      })
    } else {
      this.include_replacements.push({
        lines: [],
        into: this.parents[parentDepth - 1],
        index: this.lineno - 2,
        replace: this.$$include_directive_line,
      })
    }
    return result
  })

  Opal.defn(scope, '$push_include', function pushInclude (data, file, path, lineno, attrs) {
    this.$$push_include_called = true
    const incLineno = this.lineno - 1
    const prevIncDepth = this.include_stack.length
    // Q: can we do this without resetting the lineno?
    lineno = 1
    Opal.send(this, Opal.find_super_dispatcher(this, 'push_include', pushInclude), [data, file, path, lineno, attrs])
    const newIncDepth = this.include_stack.length
    let parentDepth = this.parents.length
    // push_include did not push to the stack
    if (newIncDepth === prevIncDepth) {
      const depthChange = newIncDepth - (parentDepth - 1)
      if (depthChange < 0) parentDepth -= this.parents.splice(parentDepth + depthChange, -depthChange).length
      this.include_replacements.push({
        lines: [],
        into: this.parents[parentDepth - 1],
        index: incLineno - 1,
        replace: this.$$include_directive_line,
      })
    } else {
      const depthChange = newIncDepth - parentDepth
      if (depthChange > 0) {
        this.parents.push(this.include_replacements.length - 1)
        parentDepth++
      } else if (depthChange < 0) {
        parentDepth -= this.parents.splice(parentDepth + depthChange, -depthChange).length
      }
      this.include_replacements.push({
        lines: this.lines.slice(),
        into: this.parents[parentDepth - 1],
        index: incLineno - 1,
        replace: this.$$include_directive_line,
      })
    }
    return this
  })

  return scope
})()

function preprocessor () {
  this.process((doc, reader) => {
    doc.$singleton_class().$prepend(DocumentExt)
    if (doc.getOptions().coalesced) return
    doc.sourcemap = true
    const { LoggerManager, NullLogger } = Opal.module(null, 'Asciidoctor')
    LoggerManager.original_logger = LoggerManager.logger
    LoggerManager.logger = NullLogger.$new()
    reader.$singleton_class().$prepend(PreprocessorReaderExt)
    reader.include_replacements = []
    reader.parents = [-1]
  })
}

function treeProcessor () {
  this.process((doc) => {
    if (doc.getOptions().coalesced) return
    const Asciidoctor = Opal.module(null, 'Asciidoctor')
    const { LoggerManager } = Asciidoctor
    const includeReplacements = doc.reader.include_replacements
    if (includeReplacements.length) {
      const resolvedSourceLines = doc.getSourceLines().slice()
      includeReplacements
        .slice()
        .reverse()
        .forEach(({ into, index, lines, replace }) => {
          // when into is -1, it indicates this is a top-level include
          const targetLines = into < 0 ? resolvedSourceLines : includeReplacements[into].lines
          // adds extra bit of assurance that we're replacing the correct line
          if (targetLines[index] === replace) targetLines.splice(index, 1, ...lines)
        })
      // WARNING: if include directives remain that can still be resolved, the sourcemap won't match the source lines
      doc = Asciidoctor.load(resolvedSourceLines, doc.options.$merge(Opal.hash({ sourcemap: true, coalesced: true })))
    }
    LoggerManager.logger = LoggerManager.original_logger
    return doc
  })
}

function register (registry) {
  if (!Array['$include?'](ArrayExt)) Array.$prepend(ArrayExt)
  registry.preprocessor(preprocessor)
  registry.treeProcessor(treeProcessor)
}

module.exports.register = register
