'use strict'

const camelCaseKeys = require('camelcase-keys')
const expandPath = require('@antora/expand-path-helper')
const { promises: fsp } = require('fs')
const ospath = require('path')
const yaml = require('js-yaml')

async function loadPdfConfig (playbook) {
  const pdfConfigPath = ospath.join(playbook.dir, 'pdf-config.yml')
  return fsp
    .access(pdfConfigPath)
    .then(() => true)
    .catch(() => false)
    .then((exists) => (exists ? fsp.readFile(pdfConfigPath).then((data) => yaml.safeLoad(data)) : {}))
    .then((config) => {
      if (config.disable) return undefined
      config = camelCaseKeys(config, { deep: true, stopPaths: ['asciidoc'] })
      if (!config.asciidoc) {
        config.asciidoc = { attributes: { doctype: 'book' } }
      } else if (!config.asciidoc.attributes) {
        config.asciidoc.attributes = { doctype: 'book' }
      }
      config.asciidoc.attributes.revdate = new Date().toISOString().split('T')[0]
      config.buildDir = expandPath(config.buildDir || './build/pdf', '~+', playbook.dir)
      if (!('clean' in config)) config.clean = playbook.output.clean
      if (!config.command) config.command = 'bundle exec asciidoctor-pdf'
      if (typeof config.componentVersions === 'string') config.componentVersions = config.componentVersions.split(', ')
      // Q: should we use process.chdir instead, or run expand path on certain config keys?
      config.cwd = playbook.dir
      if (!('insertStartPage' in config)) config.insertStartPage = true
      if (!config.processLimit) config.processLimit = 1
      if (!('publish' in config)) config.publish = true
      if (['discrete', 'fuse', 'enclose'].indexOf(config.sectionMergeStrategy) < 0) {
        config.sectionMergeStrategy = 'discrete'
      }
      return config
    })
}

module.exports = loadPdfConfig
